import numpy as np
import cv2 as cv
import os, sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/Helpers')
from helpers import *



# Load "1.jpg" as {1:M}, trimming black border: 150x150 -> 148x148.
DIR_SINGLES = 'Data/Singles'
img_dict = dict((int(i.split('.')[0]),
                cv.imread(os.path.join(DIR_SINGLES,i),0)[1:-1, 1:-1])
                for i in os.listdir(DIR_SINGLES))

def generate_dice_grid(img_dict, n_dice_h, n_dice_w):
    value_array = np.random.randint(1, 7, n_dice_h*n_dice_w)
    value_grid = value_array.reshape(n_dice_h, n_dice_w)
    img_grid = []
    for row in value_grid:
        img_array = []
        for i in row:
            img = img_dict[i]
            if i in (2, 3, 6) and np.random.random() > 0.5:
                img = np.rot90(img)
            img_array.append(img)
        img_grid.append(np.hstack(img_array))
    img_grid = np.vstack(img_grid)
    return img_grid

N_DICE_H, N_DICE_W = 10, 15
img_grid = generate_dice_grid(img_dict, N_DICE_H, N_DICE_W)

DIR_MULTI = 'Data/Multiples'
SAVENAME = '{}/{}x{}.jpg'.format(DIR_MULTI, N_DICE_H, N_DICE_W)
#pltimshow(img_grid, save=True, savename=SAVENAME)
cv.imwrite(SAVENAME, img_grid)
