# Pip Painter

I stumbled upon a [gif](https://www.reddit.com/r/specializedtools/comments/an0s16/dice_dotting_mech/) 
of a robotic arm painting the pips (dots) of a rack of
dice. The movement appeared to almost-randomly choose which pip to paint next.
I intuited that it chose the nearest neighbor of the previously-painted pip.
I think I was right.

I found a clean set of dice images online and created a n x m grid of them, randomly rotated 
(note that 1s, 4s, 5s do not need to be rotated!). Using a simple contour and ellipse finder via OpenCV,
my "robot" discovered all pips. I guess I'm assuming some perfect illumination. 
Maybe in later work I'll simulate poor and/or dynamic lighting.

By creating a map of all detected pips, and removing them from the set once painted,
the robot could jump from one pip to another in a very simple way without resorting to some
NP-hard solution. I've chosen the initial pip to be at the top left corner. 
I'm sure I could also experiment with various starting positions (knowing me, via MC)
for optimization.

Below is the path of my robot. The path is shown as a red arrow.


![](Results/10x15_path.jpg)

