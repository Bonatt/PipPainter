import numpy as np
import cv2 as cv
import os, sys
sys.path.append('/home/joshua/FunCode/MachineLearning/ComputerVision/Helpers')
from helpers import *

DIRPATH = 'Data/Multiples'
FILENAME = '2x3.jpg'
filepath = os.path.join(DIRPATH, FILENAME)

img3 = cv.imread(filepath)
img = cv.cvtColor(img3, cv.COLOR_BGR2GRAY)
# Floor/ceiling to 0/255
_, img = cv.threshold(img, img.max()//2, img.max(), cv.THRESH_BINARY)


### Blank (black) images for masking.
blank = np.zeros(img.shape, dtype=np.uint8)
blank3 = np.zeros(img3.shape, dtype=np.uint8)


### Edges from img
#edges = cv.GaussianBlur(img, (5,5), 0)
#edges = Canny(edges)
#edges3 = cv.cvtColor(edges.copy(), cv.COLOR_GRAY2BGR)

### Contours from edges
contours, hierarchy = cv.findContours(img, cv.RETR_LIST, cv.CHAIN_APPROX_SIMPLE)
#contours3 = cv.drawContours(blank3.copy(), contours, -1, GREEN)

### Ellipses from contours
ellipses = [cv.fitEllipse(i) for i in contours if cv.contourArea(i) < np.prod(img.shape)//2]
ellipses3 = img3.copy()
centers = []
for e in ellipses:
    center = tuple(int(i) for i in e[0])
    centers.append(center)
    cv.circle(ellipses3, center, 1, pltGREEN, -1)
    cv.ellipse(ellipses3, e, pltRED)
centers = np.array(centers)
#pltimshow(np.hstack([img3, ellipses3]))
#pltimshow(ellipses3)



def nearest_neighbor(centers, p0=(0,0), p=2):
    centers_ordered = []
    distances = []
    p0 = np.array(p0)
    for i in range(len(centers)):
        closest = np.linalg.norm(centers-p0, ord=p, axis=1).argmin()
        p1 = centers[closest]
        centers_ordered.append(p1)
        distances.append(np.linalg.norm(p1-p0, ord=p))
        centers = np.delete(centers, closest, axis=0)
        p0 = p1
    return np.array(centers_ordered), np.array(distances)

centers_ordered, distances = nearest_neighbor(centers)

centers_ordered_shift = np.vstack([[0,0], centers_ordered[:-1]])
for p1, p2, d in zip(centers_ordered_shift, centers_ordered, distances):
    cv.arrowedLine(ellipses3, tuple(p1), tuple(p2), pltBLUE) 
    #cv.putText()

pltimshow(ellipses3, title='Total Distance: {} pixels'.format(sum(distances)))
cv.imwrite('Results/{}'.format(FILENAME.replace('.', '_path.')), ellipses3)



#'''
plt.figure()
#x = centers_ordered[:,0]
#y = centers_ordered[:,1]
#plt.plot(x, y, '-o')
plt.plot(*centers_ordered.T, '-o', markeredgecolor='k', markerfacecolor='w', markersize=20)
for i, (x,y) in enumerate(centers_ordered):
    #plt.annotate(str(i), (x, y))
    plt.text(x, y, str(i), ha='center', va='center')
plt.show()
'''
plt.figure()
for (x1,y1), (x2,y2) in zip(centers_ordered_shift, centers_ordered):
    plt.quiver(x1,y1, x2,y2)
plt.show()
'''
